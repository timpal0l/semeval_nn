import keras
import keras.backend as K
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub

from keras.layers import Input, Lambda, Dense, Dropout, Concatenate
from keras.models import Model
from sklearn import preprocessing

url = "https://tfhub.dev/google/universal-sentence-encoder-large/3"
embed = hub.Module(url, trainable=True)

import pandas as pd
from numpy import mean, var
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import FeatureUnion, make_pipeline

data = pd.read_csv('test_data.csv', delimiter='\t', encoding='latin-1')
x1 = data['text']
x2 = data['title']
y = data['hyperpartisan']

"""le = preprocessing.LabelEncoder()
le.fit(y)"""


def encode(le, labels):
    enc = le.transform(labels)
    # return enc
    return keras.utils.to_categorical(enc)


def decode(le, one_hot):
    dec = np.argmax(one_hot, axis=1)
    return le.inverse_transform(dec)


"""x1_encoded = x1
x2_encoded = x2
y_encoded = encode(le, y)

X1 = np.asarray(x1_encoded)
X2 = np.asarray(x2_encoded)
y = np.asarray(y_encoded)"""


def UniversalEmbedding(x):
    return embed(tf.squeeze(tf.cast(x, tf.string)))


def create_model():
    input_text = Input(shape=(1,), dtype=tf.string)
    x1 = Lambda(UniversalEmbedding, output_shape=(512,))(input_text)
    x1 = Dropout(0.5)(x1)
    x1 = Dense(256, activation='relu')(x1)
    # x1 = Dropout(0.5)(x1)
    #  x1 = Dropout(0.2)(x1)
    #  x1 = Dense(128, activation='relu')(x1)

    input_title = Input(shape=(1,), dtype=tf.string)
    x2 = Lambda(UniversalEmbedding, output_shape=(512,))(input_title)
    x2 = Dropout(0.5)(x2)
    x2 = Dense(256, activation='relu')(x2)

    out = Concatenate()([x1, x2])
    drop_cat = Dropout(0.5)(out)

    pred = Dense(2, activation='softmax')(drop_cat)
    model = Model(inputs=[input_text, input_title], outputs=pred)

    # optimizer = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)

    model.compile(loss='categorical_crossentropy', optimizer="adagrad", metrics=['accuracy'])

    return model


scores = []

for i in range(0, 20):
    x1_train, x1_test, y1_train, y_test = train_test_split(x1, y, random_state=i, test_size=0.1, shuffle=True)
    x2_train, x2_test, y2_train, _ = train_test_split(x2, y, random_state=i, test_size=0.1, shuffle=True)

    le = preprocessing.LabelEncoder()
    le.fit(y1_train)
    y_encoded = encode(le, y1_train)
    y1_train_encoded = np.asarray(y_encoded)

    estimator1 = LogisticRegression(C=1.4)
    # estimator1 = LinearSVC(C=0.38)
    word_vectorizer_1 = TfidfVectorizer(analyzer='word', ngram_range=(1, 2), sublinear_tf=True, min_df=3,
                                        lowercase=True, use_idf=True, max_features=1000)
    char_vectorizer_1 = TfidfVectorizer(analyzer='char', ngram_range=(1, 2), sublinear_tf=True, min_df=3,
                                        lowercase=True, use_idf=True, max_features=1000)
    vectorizer_1 = FeatureUnion([
        ('characters', char_vectorizer_1),
        ('function_words', word_vectorizer_1),
    ])

    pipeline_x1 = make_pipeline(vectorizer_1, estimator1)
    pipeline_x1.fit(x1_train, y1_train)

    with tf.Session() as session:
        K.set_session(session)
        session.run(tf.global_variables_initializer())
        session.run(tf.tables_initializer())
        nn = create_model()
        nn.fit([np.asarray(x1_train), np.asarray(x2_train)], y1_train_encoded, epochs=5, batch_size=10, verbose=1)
        predictions_x1 = pipeline_x1.predict_proba(x1_test)
        predictions_x2 = nn.predict([np.asarray(x1_test), np.asarray(x2_test)], batch_size=10)
        print(predictions_x2)
        exit()
        del(nn)
        #print(predictions_x1)
        #print(predictions_x2)

    predictions = []

    for pred1, pred2 in zip(predictions_x1, predictions_x2):
        pred1_false, pred1_true = pred1[0], pred1[1]
        pred2_false, pred2_true = pred2[0], pred2[1]

        poss = pred1_true + pred2_true
        negs = pred1_false + pred2_false

        if poss > negs:
            prediction = 1
        elif negs > poss:
            prediction = 0

        predictions.append(prediction)

    scores.append(f1_score(y_test, predictions, average='weighted'))
    print(scores)

print('mean')
print(mean(scores))
print('var')
print(var(scores))
